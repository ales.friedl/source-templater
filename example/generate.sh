#!/bin/bash

SCRIPT_DIR=$(dirname "$(readlink -f "$0")")

source_templater="$SCRIPT_DIR/../bin/source-templater"

for template in template/*.template; do
	output=${template#template/}
	output=${output%.template}
	"$source_templater" --config "../bin/source-templater.conf.example" --template "$template" --output "$output" --force
done

"$source_templater" --list

# vim: set foldmethod=marker foldmarker=\ {{{,\ }}} foldclose= foldcolumn=0 :
