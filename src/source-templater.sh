#!/bin/bash

#================================================================
#% SYNOPSIS
#+        ${SCRIPT_NAME} [-t template] [-o output_file]
#%
#% DESCRIPTION
#%        Generate souce file skeleton using template(s).
#%
#% OPTIONS
#%       --noediting
#%               Do
#%
#%       -t TEMPLATE, --template=TEMPLATE
#%               Set template file (default=/dev/stdin if not found in
#%                                  $HOME/.source-templater/ as
#%                                  <somename>.<outputextension>.template)
#%
#%       -o OUTPUT, --output=OUTPUT
#%               Set output file (default=/dev/stdout)
#%
#%       -s SKIP, --skip=SKIP
#%               Skip SKIP in namespaces
#%
#%       -r "foo bar", --replace="foo bar"
#%               Replace foo with bar in namespaces
#%
#%       -p PREFIX, --prefix=PREFIX
#%               Set PREFIX for template placeholders (default=@@)
#%
#%       -P POSTFIX, --postfix=POSTFIX
#%               Set POSTFIX for template placeholders (default=@@)
#%
#%       -c CONFIG, --config=CONFIG
#%               Set configuration file
#%               (default="source-templater.conf" or "$HOME/.config-templater.conf")
#%
#%       -m, --mkdir
#%               Create output directory if does not exist
#%
#%       -f, --force
#%               Create output file even if nonempty output file exists
#%
#%       -l, --list
#%               List available placeholders with replacements and exit.
#%               Output could be parsed/sourced to integrate with
#%               other tools (experimental)
#%
#%       -h, --help
#%               Print help and exit successfully
#%
#%       -v, --verbose
#%               Be verbose
#%
#%       -q, --quiet
#%               Be quiet (even if verbose is set)
#%
#%       --version
#%               Print script information
#%
#% EXAMPLES
#%        ${SCRIPT_NAME} -t template.cc -f main.cc
#%
#================================================================
#- IMPLEMENTATION
#-        version         ${SCRIPT_NAME} (ash) 0.1
#-        author          Aleš Friedl
#-        copyright       Copyright (c) Aleš Friedl
#-        license         GNU General Public License
#================================================================

#================================================================
#  HISTORY
#         2018/08/11 : ash : First version
#================================================================
#  DEBUG OPTION
#        set -n  # Uncomment to check your syntax, without execution.
#        set -x  # Uncomment to debug this shell script
#
#================================================================

SCRIPT_DIR=$(dirname "$(readlink -f "$0")")
#SCRIPT_FULLNAME="${SCRIPT_DIR}${SCRIPT_DIR:+/}$0"

HAS_MAPFILE_D="$(mapfile -d "" <<<"" && echo "true")"

# Options {{{

# Default options {{{

function default_options() {
	debug=false
	verbose=false
	quiet=false
	help=false
	list=false
	force=false
	makedir=false
	version=false
	show_usage=false
	declare -a skip
	declare -a replace

	output="/dev/stdout"
	template=""
	prefix="@@"
	postfix="@@"
	for c in \
		"./.source-templater.conf" \
		"$SCRIPT_DIR/source-templater.conf" \
		"$HOME/.source-templater.conf"
	do
		if [[ -f "$c" ]]; then
			config="$c"; break;
		fi
	done
}

# }}}
# Parse   options {{{

function parse_options() {
	options=$#
	opts=""
	leave=false

	while [[ $# -gt 0 && "$leave" == "false" ]]; do
		case "$1" in
			-d|--debug)
				debug=true
				;;
			-v|--verbose)
				verbose=true
				;;
			-q|--quiet)
				quiet=true
				;;
			-l|--list)
				list=true
				;;
			-f|--force)
				force=true
				;;
			-m|--mkdir)
				makedir=true
				;;
			-x|--pretend)
				fake_output="/dev/stdout"
				;;
			-t|--template|--template=*)
				if [[ "$1" =~ ^-.*=.*$ ]]; then template=${1#*=};
				else opts="$opts $1"; shift; template="$1"; fi
				;;
			-o|--output|--output=*)
				if [[ "$1" =~ ^-.*=.*$ ]]; then output=${1#*=};
				else opts="$opts $1"; shift; output="$1"; fi
				;;
			-s|--skip|--skip=*)
				if [[ "$1" =~ ^-.*=.*$ ]]; then skip+=("${1#*=}");
				else opts="$opts $1"; shift; skip+=("$1"); fi
				;;
			-r|--replace|--replace=*)
				if [[ "$1" =~ ^-.*=.*$ ]]; then replace+=("${1#*=}");
				else opts="$opts $1"; shift; replace+=("$1"); fi
				;;
			-p|--prefix|--prefix=*)
				if [[ "$1" =~ ^-.*=.*$ ]]; then prefix=${1#*=};
				else opts="$opts $1"; shift; prefix="$1"; fi
				;;
			-P|--postfix|--postfix=*)
				if [[ "$1" =~ ^-.*=.*$ ]]; then postfix=${1#*=};
				else opts="$opts $1"; shift; postfix="$1"; fi
				;;
			-c|--config|--config=*)
				if [[ "$1" =~ ^-.*=.*$ ]]; then config=${1#*=};
				else opts="$opts $1"; shift; config="$1"; fi
				;;
			--version)
				version=true
				;;
			-h|--help)
				help=true
				;;
			--)
				leave=true
				;;
			-*)
				printf "%s %s\\n" "Unknown argument" "$1" >&2
				show_usage=true
				;;
			*)
				leave=true
				;;
		esac
		if ! $leave; then opts="$opts $1"; fi
		if ! $leave; then shift; fi
	done;
	to_shift=$((options - $#))
}

# }}}
# Process options {{{

function process_options() {
	#set_colors $colors

	if $quiet; then
		exec 1>/dev/null
		exec 2>/dev/null
	fi
	if $show_usage; then
		usage
		exit 1
	fi

	if $help; then
		usage
		exit 0
	fi

	if $version; then
		usage_version
		exit 0
	fi

	local -r output_ext="${output##*.}"
	if [[ -n  "$output_ext" ]] &&
	   [[ -z "$template" ]]
	then
		for t in \
			"$HOME/.source-templater/"*".$output_ext.template"
		do
			if [[ -f "$t" ]]; then
				template="$t"; break;
			fi
		done
	fi
	if [[ -z "$template" ]]; then
		template="/dev/stdin"
	fi
}

# }}}

function options() {
	default_options
	parse_options "$@"
	process_options
}

# }}}
# Usage {{{

usage() {
	# if [[ "$verbose" == "true" ]]; then
	 	sed -n -e "2,/^$/ s/^#[%+] \\(.*\\)\$/\\1/p" -- "${0}" | sed -e "s/\${SCRIPT_NAME}/$(basename "${0}")/g"
	# else
	# 	printf "Usage: "; sed -n -e "2,/^$/ s/^#+ \\(.*\\)\$/\\1/p" -- "${0}" | sed -e "s/\${SCRIPT_NAME}/$(basename "${0}")/g"
	# fi
}

usage_version() {
	sed -n -e "2,/^$/ s/^#- \\(.*\\)\$/\\1/p" -- "${0}" | sed -e "s/\${SCRIPT_NAME}/$(basename "${0}")/g"
}

# }}}

options "$@"
if [[ "$to_shift" -gt 0 ]]; then
	shift "$to_shift"
	to_shift=0
fi
if [[ $# -gt 0 ]]; then
	printf "%s %s\\n" "Unknown argument" "$1" >&2
	exit 1
fi

declare -A placeholders
add_placeholder() {
	local k=$1
	local v=$2
	local RETVAL # do not use global RETVAL
	# shellcheck disable=SC2034
	placeholders["$prefix$k$postfix"]="$v"
}

if [[ -r "$config" ]]; then
	# shellcheck source=source-templater.conf disable=1091 # best-effort
	. "$config"
	msc="$config"
fi

snake2pascal() {
    parts=${1//_/\ }
    parts=${parts//./\ }
    # shellcheck disable=SC2206 
    parts=($parts) # mapfile -d "_" -t parts <<<"$1"
    printf -v RETVAL "%s" "${parts[@]^}"
}

snake2camel() {
	snake2pascal "$1"
	RETVAL=${RETVAL,}
}

camel2snakecamel() {
	RETVAL=$(LANG=C sed 's/\(\B[A-Z]\)/_\1/g' <<<"$1")
}

if [[ -x "$(command -v uuidgen)" ]]; then
	uuidgen_() {
		# UUID http://www.ietf.org/rfc/rfc4122.txt (Jan Zima, CZ.NIC)
		uuidgen | tr -d "-" # | tr "a-f" "A-F"
	}
else
	uuidgen_() {
    	# Michal Strnad, CZ.NIC
		# date "+%s" | md5sum | tr "[a-f]" "[A-F]" | cut -d" " -f 1
    	# Miroslav Franc, CZ.NIC mod of Michal Strnad, CZ.NIC
		# head -c 20 /dev/urandom | md5sum | tr "a-f" "A-F" | cut -d" " -f 1
		head -c 20 /dev/urandom | md5sum | cut -d" " -f 1 # | tr "a-f" "A-F"
	}
fi

headerguardgen_uuid() {
	local -r file_name="$1"
	local headerguard="$file_name"
	local uuid
	headerguard=${headerguard//\//_}
	headerguard=${headerguard//\./_}
	headerguard=${headerguard//-/_}
	headerguard=${headerguard^^}
	uuid=$(uuidgen_)
	uuid=${uuid^^}
	RETVAL="${headerguard}_${uuid}"
}

headerguardgen_relative() {
	local -r file_relative="$1"
	local headerguard="$file_relative"
	headerguard=${headerguard//\//_}
	headerguard=${headerguard//\./_}
	headerguard=${headerguard//-/_}
	headerguard=${headerguard^^}
	RETVAL="${headerguard}"
}

headerguardgen_filename() {
	local -r file_name="$1"
	local headerguard="$file_name"
	headerguard=${headerguard//\//_}
	headerguard=${headerguard//\./_}
	headerguard=${headerguard//-/_}
	headerguard=${headerguard^^}
	RETVAL="__${headerguard}__"
}

declare -A RETVAL_ARR
path2ns() {
    local -r conversion="$1"; shift
    local namespace
    local footernamespace
    local header
    local footer
    IFS="/" read -ra directories <<<"$@"
    for dir in "${directories[@]}"; do
        "$conversion" "$dir"
        namespace="$RETVAL"
        header+="${header:+\n}"
        header+="namespace $namespace {"
        footernamespace+="${footernamespace:+::}$namespace"
        footer="} // namespace $footernamespace${footer:+\\n$footer}"
    done
    RETVAL_ARR["{"]="$header"
    RETVAL_ARR["}"]="$footer"
}

list_arr() {
	local -n a=$1
	local -r a_name=$1
	if [[ "$HAS_MAPFILE_D" == "true" ]]; then
		mapfile -d '' sorted_keys < <(printf '%s\0' "${!a[@]}" | sort -z)
	else
		# $BASH_VERSION < 4.4
		declare -a sorted_keys
		while IFS=$'\0' read -r key; do
			sorted_keys+=("$key")
		done < <(printf '%s\n' "${!a[@]}" | sort)
		# OR
		#local sorted_keys=($(printf '%s\n' "${!a[@]}" | sort))
	fi
	for key in "${sorted_keys[@]}"; do
		printf "%s[\"%s\"]=\"%s\"\\n" "$a_name" "$key" "${a[$key]}"
	done
	exit 0
}

transform() {
	local -n d=$1
	local -n a=$3
	# important to reverse-sort if postfix == ""
	if [[ "$HAS_MAPFILE_D" == "true" ]]; then
		mapfile -d '' reverse_sorted_keys < <(printf '%s\0' "${!a[@]}" | sort -r -z)
	else
		# $BASH_VERSION < 4.4
		declare -a reverse_sorted_keys
		while IFS=$'\0' read -r key; do
			reverse_sorted_keys+=("$key")
		done < <(printf '%s\n' "${!a[@]}" | sort -r)
		# OR
		#local reverse_sorted_keys=($(printf '%s\n' "${!a[@]}" | sort -r))
	fi
	for key in "${reverse_sorted_keys[@]}"; do
		#d=${d//${key////\/}/"${a[$key]}"}
		d=${d//${key}/"${a[$key]}"}
	done
}

file_relative="$output"
file_name="${file_relative##*/}"
file_relative_path="${file_relative%$file_name}"
file_relative_path="${file_relative_path%/}"
file_ext=${file_name##*.}
##file_basename="${file_name%.*}"
file_basename="$(basename "$file_relative" ".$file_ext")"

add_placeholder "FILE_BASE" "$file_basename"
add_placeholder "FILE_NAME" "$file_name"
add_placeholder "FILE_EXT" "$file_ext"
add_placeholder "FILE_DIR" "$file_relative_path"
add_placeholder "FILE_DIR_AND_BASE" "$file_relative_path${file_relative_path:+/}$file_basename"
add_placeholder "FILE_DIR_AND_NAME" "$file_relative"

snake2pascal "$file_basename"
add_placeholder "FILE_BASE_PASCAL_CASE" "$RETVAL"

snake2camel "$file_basename"
add_placeholder "FILE_BASE_CAMEL_CASE" "$RETVAL"

camel2snakecamel "$file_basename"
add_placeholder "FILE_BASE_SNAKE_CASE" "$RETVAL"
add_placeholder "FILE_BASE_SNAKE_LOWER_CASE" "${RETVAL,,}"
add_placeholder "FILE_BASE_SNAKE_UPPER_CASE" "${RETVAL^^}"

add_placeholder "DATE_YYYY" "$(date "+%Y")"

headerguardgen_uuid "$file_name"
add_placeholder "HEADERGUARD_UUID" "$RETVAL"
add_placeholder "HEADERGUARD_HASH" "$RETVAL"

headerguardgen_relative "${file_relative#/}"
add_placeholder "HEADERGUARD_PATH" "$RETVAL"

headerguardgen_filename "$file_name"
add_placeholder "__HEADERGUARD__" "$RETVAL"

namespaces_path="$file_relative_path"
for s in "${skip[@]}"; do
	# shellcheck disable=SC2001
	namespaces_path="$(echo "$namespaces_path" | sed -e "s|\\/\\?\\b$s\\b||g")"
done
for r in "${replace[@]}"; do
	# shellcheck disable=SC2001
	namespaces_path="$(echo "$namespaces_path" | sed -e "s|\\b${r%%\ *}\\b|${r##*\ }|g")"
done
namespaces_path=${namespaces_path##/}

path2ns "snake2pascal" "$namespaces_path"
add_placeholder "NAMESPACES_PASCAL_CASE_{" "${RETVAL_ARR["{"]}"
# shellcheck disable=SC2034
add_placeholder "NAMESPACES_PASCAL_CASE_}" "${RETVAL_ARR["}"]}"

path2ns "camel2snakecamel" "$namespaces_path"
add_placeholder "NAMESPACES_SNAKE_CASE_{" "${RETVAL_ARR["{"]}"
add_placeholder "NAMESPACES_SNAKE_CASE_}" "${RETVAL_ARR["}"]}"
add_placeholder "NAMESPACES_SNAKE_LOWER_CASE_{" "${RETVAL_ARR["{"],,}"
add_placeholder "NAMESPACES_SNAKE_LOWER_CASE_}" "${RETVAL_ARR["}"],,}"

if [[ "$debug" != "false" ]]; then
	echo
	echo "DEBUG"
	echo "file_relative:      == $file_relative"
	echo "file_relative_path: == $file_relative_path"
	echo "file_basename:      == $file_basename"
	echo "file_name:          == $file_name"
	echo "file_ext:           == $file_ext"
	echo "namespaces_path:    == $namespaces_path"
	echo "config:             == $config"
fi

if $list; then
	list_arr placeholders
	exit 0
fi

data=$(cat "$template")

transform data using placeholders and stay cool

if [[ -s "$file_relative" ]]; then
	if [[ "$force" != "true" ]]; then
		printf "%s error: nonempty target file exists (%s)\\n" "$(basename "$0")" "$file_relative" >&2
		exit 1
	fi
fi
if [[ "${fake_output:-}" == "" ]]; then # real run
	if [[ ! -d "${file_relative_path:-.}" ]]; then
		if [[ "$makedir" == "true" ]]; then
			mkdir -p "$file_relative_path"
		else
			printf "%s error: target directory does not exist (%s) and --mkdir not set\\n" "$(basename "$0")" "$file_relative_path" >&2
			exit 1
		fi
	fi
fi
printf "%b" "$data" > "${fake_output:-$file_relative}"
