.PHONY: default all build build_test build_third_party install test unittest log_dir clean clean_build clean_install

PROJECT = source-templater
LOG_DIR = build/log
THIRD_PARTY = bunit

default: all

all: install build_test

build: build/bin/${PROJECT} build/bin/${PROJECT}.conf

build/bin/${PROJECT}: src/${PROJECT}.sh
	@mkdir -p ${@D}
	@cat $< >$@
	@chmod a+x $@

build/bin/${PROJECT}.conf: src/${PROJECT}.conf
	@mkdir -p ${@D}
	@cp $< $@

build/lib/%: src/lib/%.sh
	@mkdir -p ${@D}
	@cp $< $@

build_test: build build_third_party
	@cp -r test build
	@find build/test -type f -name '*.sh' -exec chmod \+x {} +

build_third_party:
	@mkdir -p "build"
	@cp -r third_party build
	@cd build/third_party/${THIRD_PARTY} && make build

install: build bin/${PROJECT} bin/${PROJECT}.conf.example

bin/${PROJECT}: build/bin/${PROJECT}
	@mkdir -p ${@D}
	@cp $< $@

bin/${PROJECT}.conf.example: build/bin/${PROJECT}.conf
	@mkdir -p ${@D}
	@cp $< $@

unittest: build_test log_dir
	@BUNIT_COLORS=false build/test/run.sh

test: unittest

log_dir:
	@mkdir -p ${LOG_DIR}

clean: clean_build clean_install

clean_build:
	@rm -fr build

clean_install:
	@rm -fr bin
