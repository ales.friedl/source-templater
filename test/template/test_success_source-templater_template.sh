#!/bin/bash

test_success_source-templater_template() {
	filepath="src/registrable_object"
	for template in template/fixture/template/*; do
		file="${template##*/}"
		setup
		rm -f "$filepath/$file" || return 1
		setup_end
		assert_success <"$template" "$PROJECT_ROOT/bin/source-templater" \
			--template "/dev/stdin" \
			--output "$filepath/$file" \
			--skip "registrable_object" \
			--replace "src project/alpha" \
			--config "$PROJECT_ROOT/bin/source-templater.conf" \
			--mkdir
		[[ -s "$filepath/$file" ]] || return 1
		assert_failure grep "@@" "$filepath/$file"
	done
}

test_success_source-templater_template_noskip() {
	filepath="src/nonregistrable_object/src"
	for template in template/fixture/template/*; do
		file="${template##*/}"
		setup
		rm -f "$filepath/$file" || return 1
		setup_end
		assert_success <"$template" "$PROJECT_ROOT/bin/source-templater" \
			--template "/dev/stdin" \
			--output "$filepath/$file" \
			--skip "registrable_object" \
			--replace "src project/alpha" \
			--config "$PROJECT_ROOT/bin/source-templater.conf" \
			--mkdir
		[[ -s "$filepath/$file" ]] || return 1
		assert_failure grep "@@" "$filepath/$file"
	done
}
