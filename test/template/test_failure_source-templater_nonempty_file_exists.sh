#!/bin/bash

test_failure_source-templater_nonempty_file_exists() {
	filepath="src/registrable_object"
	for template in template/fixture/template/*; do
		file="${template##*/}"
		setup
		mkdir -p "$filepath"
		echo "not empty" > "$filepath/$file" || return 1
		md5sum_before=$(md5sum "$filepath/$file")
		setup_end
		assert_failure <"$template" "$PROJECT_ROOT/bin/source-templater" \
			--template "/dev/stdin" \
			--output "$filepath/$file" \
			--skip "registrable_object" \
			--replace "src project" \
			--config "$PROJECT_ROOT/bin/source-templater.conf" \
			--mkdir || return 1
		md5sum_after=$(md5sum "$filepath/$file")
		assert_equal "$md5sum_before" "$md5sum_after"
	done
}
