@@PROJECT_LICENSE@@
/**
 * @file @@FILE_NAME@@ = @@FILE_BASE@@ + "." + @@FILE_EXT@@
 */
#ifndef @@HEADERGUARD_UUID@@
#define @@HEADERGUARD_HASH@@
#define @@HEADERGUARD_PATH@@
#define @@__HEADERGUARD__@@
@@NAMESPACES_PASCAL_CASE_{@@
// something in the namespace
@@NAMESPACES_PASCAL_CASE_}@@
#endif
