test_success_source-templater_function_camel2snakecamel() {
	# shellcheck disable=SC1090
	. "$PROJECT_ROOT/bin/source-templater" </dev/null

	declare -A data
	data=(
		[camelCase]=camel_Case
		[lower]=lower
		[PascalCase]=Pascal_Case
		[Pascal]=Pascal
		[1amel2ase]=1amel2ase
	)
	for value in "${!data[@]}"; do
		printf "Trying \"%s\"...\\n" "$value"
		camel2snakecamel "$value"
		assert_equal "$RETVAL" "${data[$value]}" || return 1
	done
}
