test_success_source-templater_function_snake2camel() {
	# shellcheck disable=SC1090
	. "$PROJECT_ROOT/bin/source-templater" </dev/null

	declare -A data
	data=(
		[snake_case]=snakeCase
		[snake]=snake
		[snakeCase]=snakeCase
		[SnakeCase]=snakeCase
		[1nake_2ase]=1nake2ase
		[1nake2ase]=1nake2ase
	)
	for value in "${!data[@]}"; do
		printf "Trying \"%s\"...\\n" "$value"
		snake2camel "$value"
		assert_equal "$RETVAL" "${data[$value]}" || return 1
	done
}
