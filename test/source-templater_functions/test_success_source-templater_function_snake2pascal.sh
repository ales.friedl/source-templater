test_success_source-templater_function_snake2pascal() {
	# shellcheck disable=SC1090
	. "$PROJECT_ROOT/bin/source-templater" </dev/null

	declare -A data
	data=(
		[snake_case]=SnakeCase
		[snake]=Snake
		[snakeCase]=SnakeCase
		[SnakeCase]=SnakeCase
		[1nake_2ase]=1nake2ase
		[1nake2ase]=1nake2ase
	)
	for value in "${!data[@]}"; do
		printf "Trying \"%s\"...\\n" "$value"
		snake2pascal "$value"
		assert_equal "$RETVAL" "${data[$value]}" || return 1
	done
}
