#!/bin/bash

test_failure_source-templater_nonexistent_option() {
	assert_failure "$PROJECT_ROOT/bin/source-templater" -w
	assert_failure "$PROJECT_ROOT/bin/source-templater" --nonexistent
}
