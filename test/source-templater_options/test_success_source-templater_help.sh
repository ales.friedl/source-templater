#!/bin/bash

test_success_source-templater_help_short() {
	assert_success "$PROJECT_ROOT/bin/source-templater" -h
	assert_success "$PROJECT_ROOT/bin/source-templater" -h -v
}

test_success_source-templater_help_long() {
	assert_success "$PROJECT_ROOT/bin/source-templater" --help --verbose
	assert_success "$PROJECT_ROOT/bin/source-templater" --version
}
