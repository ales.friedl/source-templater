#!/bin/bash

test_failure_source-templater_nonexistent_argument() {
	assert_failure "$PROJECT_ROOT/bin/source-templater" h
	assert_failure "$PROJECT_ROOT/bin/source-templater" nonexistent
}
