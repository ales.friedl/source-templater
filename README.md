Source templater.

Quick start:
```
" Copy templates from `example/templates` to your "$HOME/.source-templater/" directory:
mkdir "$HOME/.source-templater"
cp example/templates/\*.template "$HOME/.source-templater"

" Enter your project directory:
mkdir myproject
cd myproject

" Add project-specific configuration file named `.source-templater.conf` to your project root (the directory you open project files from):
printf 'replace+="src myproject"' > .source-templater.conf

" Generate your first source file:
source-templater --output src/myclass.cc --mkdir
```

Sample template:
```
@@NAMESPACES_PASCAL_CASE_{@@
@@NAMESPACES_PASCAL_CASE_}@@
```

Sample execution:
```
source-templater --file "src/module/submodule/util.cc" --replace "src project" --skip "module" --pretend
```

Sample output:
```cpp
namespace Project {
namespace Submodule {
} // namespace Project::Submodule
} // namespace Project
```

Help (verbose):
```
source-templater -h -v

```

List available placeholders (including those from the config file) and exit:
```
source-templater [-f path_to/myfile.cc] --list

```
_Some placeholders depend on target file path so it is why it may make sense to add it._


Vim integration (with skeleton plugin):
```
" skeleton (root.cz) - add the following line to the SKEL_replace() function:
exe "%!/path/to/source-templater --output \"" . expand("%") . "\" --prefix skeletonVIM_ --postfix \"\""
```
